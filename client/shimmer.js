let logoObject     = document.getElementById("logo");
let svgDoc         = logoObject.contentDocument;
let linearGradient = svgDoc.getElementsByTagName("linearGradient");
let deg = 0;

let animation = setInterval(() => {
    if(linearGradient.length === 0) {
        logoObject     = document.getElementById("logo");
        svgDoc         = logoObject.contentDocument;
        linearGradient = svgDoc.getElementsByTagName("linearGradient");
    }

    for ( let i = 0; i < linearGradient.length; i++ ) {
        linearGradient[i].setAttribute("gradientTransform", `rotate( ${deg} 1582 1582)`);
    }

    deg += 1;

}, 8);