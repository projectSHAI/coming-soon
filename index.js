const express = require('express')
const app = express()

app.use(express.static('assets'));

app.use(express.static('client'));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/client/index.html')
})

app.listen(8888, function () {
  console.log('Example app listening on port 8888!')
})